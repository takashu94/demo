## 1. Prepare Environment

- OS  : Window 7 or Window 10
- IDE : Eclipse oxygen2 64bit. It is for backend development.
	``` bash
	Download Link : 
	http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/oxygen2
	```
- Sublime text 3 or Visual studio code. It is for frontend development.
	``` bash
	Download Link : 
	https://www.sublimetext.com/3
	https://code.visualstudio.com/
	
	```
		
- Install Spring Tool Suite into eclipse
	``` bash
	Detail install please refer to link
	https://o7planning.org/en/10249/install-spring-tool-suite-into-eclipse
	```
	
## 2. Install Backend
	1. Open eclipse
	2. Include will-common, will-database, will-cms-api project into eclipse by steps : 
		a. Chose File -> Import -> Maven -> Existing maven project
		b. Click [Next] button
		c. At [Import Maven Projects] screen, then select [Browse...] button.
		d. Point to [will3-prototype] folder.
		e. Select will-common, will-database, will-cms-api project then click [Finish] button.
		f. Wating for finished build.
		g. Finished
	3. Run Backend project
		a. Right click on [will-cms-api] project
		b. At context menu select [run as] -> 
		
## 3. Install Database
	1. Install MySQL (It look like MySQL of wILL Community ES Version)
	2. Open folder [Database]
	3. Run file [createdb.bat] to import database schema & data

## 4. Install Frontend
	1. Open [WillCMS_Client] folder
	2. Open Sublime text 3 or Visual studio code then Import [WillCMS_Client] into them.
	3. Build Setup
	
	``` bash
	# install dependencies
	npm install

	# serve with hot reload at localhost:8080
	npm run dev

	# build for production with minification
	npm run build

	# build for production and view the bundle analyzer report
	npm run build --report
	```
	

## 5. URL

	``` bash
	Login : http://localhost:8080/
	Home : http://localhost:8080/#/home
	404 : http://localhost:8080/#/404
	500 : http://localhost:8080/#/500
	```

